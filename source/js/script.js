document.addEventListener('DOMContentLoaded', function () {
    const slider = new ChiefSlider('.slider', {
        loop: true,
        autoplay: true,
        interval: 7000,
    });
});

$(document).ready(
    function () {
    $('#menu__drawer').click(function () {
        $(this).toggleClass('open');
        $('#menu').toggleClass('menu--open');
    });
    $('#open-modal').click(function(){
        $('#modal-video').toggleClass('modal-video--open');
    })
    $('#modal-video').click(function(){
        $('#modal-video').removeClass('modal-video--open');
    })
});